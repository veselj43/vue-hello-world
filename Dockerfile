FROM node:14.18.2 as build-stage

ARG ENV

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn

COPY . .
RUN yarn build --env=${ENV}

FROM nginx:stable-alpine as runtime

COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/dist /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
