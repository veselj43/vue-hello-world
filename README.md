# hello-world

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```bash
# init local config
cp config/local.env.example.js config/local.env.js

# run
yarn dev
```

### Compiles and minifies for deploy
```bash
# prod
yarn build --env=prod

# stage
yarn build --env=stage
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
