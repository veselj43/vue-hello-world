const { argv } = require('yargs')
const { name, version } = require('./package.json')

const ENV = argv.env || 'prod'

module.exports = (() => {
  const configPath = `./config`
  const configEnv = `${configPath}/${ENV}.env`

  console.log(configEnv)

  try {
    const config = require(configEnv)

    return {
      appInfo: {
        name,
        version,
        env: ENV,
      },
      ...config,
    }
  } catch (ex) {
    console.error(ex)
    throw new Error(`Config not found: ${ex.message}`)
  }
})()
