module.exports = {
  sentry: {
    dsn: '', // leave empty to disable sentry error logging
    env: 'local',
  },
  api: {
    test: 'http://localhost:8848',
  },
}
