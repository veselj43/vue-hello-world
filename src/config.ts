const CONFIG: any = process.env.VUE_APP_CONFIG
export const CURRENT_ENV = CONFIG.appInfo.env

export const SENTRY_DSN = CONFIG.sentry.dsn
export const SENTRY_ENV = CONFIG.sentry.env
export const SENTRY_RELEASE = `${CONFIG.appInfo.name}@${CONFIG.appInfo.version}`
export const API = CONFIG.api
