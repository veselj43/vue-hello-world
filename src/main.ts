import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { init as initVuetify } from './plugins/vuetify'

Vue.config.productionTip = false

const vuetify = initVuetify(Vue)

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
