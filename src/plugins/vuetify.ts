import Vuetify from 'vuetify/lib/framework'

export const init = (vue) => {
  vue.use(Vuetify)

  return new Vuetify({
    theme: {
      options: {
        customProperties: true,
      },
      themes: {
        light: {
          primary: '#49a927',
          secondary: '#455a64',
          // secondary: '#ed174f', // drmax secondary (red)
          accent: '#b318ed',
          error: '#ff5252',
          info: '#2196f3',
          success: '#49a927',
          warning: '#fb8c00',
        },
      },
    },
  })
}
