import axios from 'axios'

import { API } from '@/config'

const apiClient = axios.create({
  baseURL: API.test,
})

apiClient.interceptors.response.use(
  (response) => response,
  (error) => {
    // check some common response statuses, codes, types, ...
    throw {
      message: error.response?.message ?? error.message,
    }
  },
)

export async function fetchThing(thingId: string): Promise<any> {
  const { data } = await apiClient.get(`/things/${thingId}`)
  return data
}
