const webpack = require('webpack')
const APP_CONFIG = require('./app.config')

console.log(`node_env: ${process.env.NODE_ENV}, config_env: ${APP_CONFIG.appInfo.env}`)

const port = 8080 // dev server port

module.exports = {
  publicPath: '/',
  configureWebpack: {
    devtool: 'source-map',
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          VUE_APP_CONFIG: JSON.stringify(APP_CONFIG),
        },
      }),
    ],
  },
  devServer: {
    // devServer settings should fix hot reload while on VPN (drmax cisco)
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    overlay: true,
    port, // This will take care of desktop machine
    public: `0.0.0.0:${port}`, // This will take care of mobile device
    disableHostCheck: true,
  },
  transpileDependencies: ['vuetify'],
}
